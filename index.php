<!doctype html>
<html>
  <head>
  <meta charset="utf-8">
    <title>Formulario de Registro SCIII</title>
    <link rel="stylesheet" href="dist/daterangepicker.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="dist/jquery.daterangepicker.min.js"></script>
  </head>
  <script src="demo.js"></script>
 
<body>

 

  
<form action="enviarDatos.php" method="post">
  <div class="container">
  <div class="row header">
    <h1>Naxos &nbsp;</h1>
    <h3>Tecnologia que mejora tu vida</h3>
  </div>
  <div class="row body">
    <form action="#">
      <ul>
        <li>
          <p class="left">
            <label for="first_name">Nombre</label>
            <input type="text" name="first_name" placeholder="Nombre" />
          </p>
          <p class="pull-right">
            <label for="last_name">Apellido</label>
            <input type="text" name="last_name" placeholder="Apellido" />      
          </p>
        </li>
        <li id="two-inputs">
          <label>Primer Día </label>
          <input id="date-range200" size="20" value="">
          <label>Ultimo Día</label>
          <input id="date-range201" size="20" value="">
        </li>
      
        <li>
          <p>
            <label for="email">Email <span class="req">*</span></label>
            <input type="email" name="email" placeholder="Correo" />
          </p>
        </li>        
        <li><div class="divider"></div></li>
        <li>
          <label for="comments">Comentarios</label>
          <textarea cols="46" rows="3" name="comments"></textarea>
        </li>
        
        <li>
          <input class="btn btn-submit" type="submit" value="Enviar" />
        </li>
      </ul>
    </form>  
  </div>
</div>

</form>


</body>

    
</html>